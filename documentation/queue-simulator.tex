\documentclass[a4paper,10pt]{article}
\usepackage{fontspec}
\usepackage[singlespacing]{setspace}
\usepackage[none]{hyphenat}
\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\setmainfont{Times New Roman}

%opening
\title{Queue Simulator}
\author{Katona Áron}

\begin{document}

    \pagenumbering{gobble}
    \maketitle
    \newpage
    \pagenumbering{roman}
    \tableofcontents
    \newpage
    \pagenumbering{arabic}
    
    \section{Objective}
    The main objective is to design and implement a simulation application aiming to analyze queuing based systems.
    \par
    Clients are generated randomly, and after their arrival time they are placed in queues, where they wait until they receive their services. The goal is to determine and minimize the average waiting time of the clients. 
    
    \subsection{Secondary objectives}
    The main objective can be divided into multiple independent steps:
    \begin{itemize}
     \item \textbf{Reading the simulation configuration from a file, and writing to another file the result} 
     \hyperref[sec:io]{[\ref{sec:io}]}
     \item \textbf{Input validation}
     \hyperref[sec:validation]{[\ref{sec:validation}]}
     \item \textbf{Designing data structures for data storage and transfer}
     \hyperref[sec:client]{[\ref{sec:client}]}
     \hyperref[sec:dto]{[\ref{sec:dto}]}
     \item \textbf{Generating clients randomly}
     \hyperref[sec:client]{[\ref{sec:client}]}
     \item \textbf{Creation of multiple threads, each representing a queue}
     \hyperref[sec:scheduler]{[\ref{sec:scheduler}]}
     \hyperref[sec:server]{[\ref{sec:server}]}
     \item \textbf{Synchronizing the threads}
     \hyperref[sec:server]{[\ref{sec:server}]}
     \item \textbf{Choosing a good strategy for dispatching the clients}
     \hyperref[sec:strategy]{[\ref{sec:strategy}]}
    \end{itemize}

    
    \section{Problem analysis and use cases}
    \subsection{Problem analysis}
    \label{sec:analysis}
    From the command line a path to a text file is given containing the following simulation constraints:
    \begin{itemize}
     \item Number of clients ($N$)
     \item Number of queues ($Q$)
     \item Time limit of the simulation ($t_{simulation}^{MAX}$)
     \item Minimum and maximum arrival time ($t_{arrival}^{MIN}$, $t_{arrival}^{MAX}$)
     \item Minimum and maximum service time ($t_{service}^{MIN}$, $t_{service}^{MAX}$)
    \end{itemize}
    
    $N$ clients are generated randomly using this configuration, and $Q$ threads are started. The clients are characterised by:
     \begin{itemize}
     \item Unique identifier ($id$): $1 \leq id \leq n$
     \item Arrival time ($t_{arrival}$): $t_{arrival}^{MIN}\leq t_{arrival} \leq t_{arrival}^{MAX}$
     \item Service time ($t_{service}$): $t_{service}^{MIN}\leq t_{service} \leq t_{service}^{MAX}$
    \end{itemize}
    
    The simulation is performed in steps, representing the simulated time. This time is the key in the synchronization of the threads: 
    \begin{itemize}
     \item Only one client can be extracted from the queue between two consecutive time pulses.
     \item It determines the real duration for which a thread is paused: the arrival and service times of the clients are represented in numbers of time pulses.
     \item The time is incremented only when all the threads finished their jobs for the current pulse. Only in this moment can we safely request the contents of the queues.
    \end{itemize}

    
    \subsection{Use cases}
    \textbf{Use Case:} Simulate a scenario of dispatching clients to queues\\
    \textbf{Primary Actor:} User\\
    \textbf{Main Success Scenario:}
    \begin{enumerate}
        \item The user navigates to the location of the jar file.
        \item \label{step:1} The user creates a configuration input file next to the jar, and fills it with values in the format described in [\ref{sec:analysis}].
        \item The user calls the program in the terminal using the following command:
        \begin{verbatim}java -jar PT2020_30423_Aron_Katona_Assignment_2.jar \
        <input_file> <output_file>\end{verbatim}
        Where the \verb|<input_file>| corresponds to the name of the file created in step \ref{step:1}, and \verb|<output_file>| is the name of the file that will be created or overwritten to store the simulation result.
        \item The program writes the result to the output file and the exits without any error message.
    \end{enumerate}

    \textbf{Alternative Sequences:}
    \begin{enumerate}
        \item File IO errors
        \begin{itemize}
            \item Occurs when a file could not be opened, written into or read from.
            \item The program exits with an error message printed to the terminal (STDERR).
        \end{itemize}
        \item The format or the content of the input file is invalid
        \begin{itemize}
            \item The program exits with an error message printed to the terminal (STDERR).
        \end{itemize}
    \end{enumerate}
    
    \section{Design}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.5\linewidth]{figures/package.png}
        \caption{Package diagram}
        \label{fig:package}
    \end{figure*}
    
    
    \par
    On figure \ref{fig:package} the relationships between the packages are shown. 
    \par
    The main logic of the simulation is placed in the \verb|simulation| package. It uses the \verb|time| package, which manages the the global time and allows classes to subscribe to the time pulses. Furthermore, the \verb|strategy| package contains the interface and the classes which  decide for a client to which server(queue) to be dispatched.
    \par
    In order to support reading and writing operations with files, additional packages are used: the \verb|io| package implements generic object <-> text-file conversions. To achieve this, the \verb|transformer| package is used, which handles the conversions. Moreover, the \verb|dto| package contains the data transfer objects, which are sent between the IO classes and the \verb|SimulationManager|. Finally, the \verb|validation| package checks the input correction.
    
    \section{Implementation}
    \subsection{Time}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.5\linewidth]{figures/time.png}
        \caption{time package}
        \label{fig:time}
    \end{figure*}
    The time has a key role in the simulation. The \verb|Time| class contains the current time in an \verb|AtomicInteger| variable, thus it can be accessed from multiple threads in parallel. 
    \par 
    Besides that, a class can subscribe to the passing of time by implementing the \verb|TimeListener| interface, and adding itself to the list of listeners in the \verb|Time| class. This is achieved using the observable pattern. When the \verb|tick()| method is called, all listeners are activated, by calling their \verb|timePassed()| method and passing to them the current time. In the end, the time is incremented.
    \par
    In order to give access to various classes to the same global time, the singleton pattern is used.
    
    \subsection{Client}
    \label{sec:client}
    \begin{figure*}[!htbp]
         \centering
         \includegraphics[width=0.8\linewidth]{figures/client.png}
         \caption{client package}
         \label{fig:client}
     \end{figure*}
    The purpose of the simulation is to transfer the \verb|Client| objects to the \verb|Server| objects, minimizing their waiting time. 
    \par
    The main fields of the \verb|Client| class presented in [\ref{sec:analysis}], are final and immutable, so no modifications can occur to them. Besides them, the class contains the \verb|waitingTime| field which is updated on enqueue to the time duration the client must wait to be received at the server. The \verb|isServed| field is set to true only after the object was process fully by a \verb|Server|. 
    \par
    The clients are generated randomly using the \verb|ClientGenerator| utility class and its \verb|generateClients()| method. The \verb|ClientComparator| class is used to sort a list of \verb|Client| objects in increasing order of arrival time and service time.
    
    
    \subsection{Simulation}
    \subsubsection{SimulationManager}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.5\linewidth]{figures/sim-manager.png}
        \caption{SimulationManager class}
        \label{fig:sim-manager}
    \end{figure*}
    The \verb|SimulationManager| class implements the \verb|Callable| interface \cite{geeks4geeks:1} because after the execution of its \verb|call()| method it returns an object of type \verb|SimulationResult| which contains all the data required for the output.
    \par
    In its constructor it gets the configuration object of type \verb|SimulationConfig|. Using the object's data, it generates random clients via the \verb|ClientGenerator|,  sorts it using the \verb|ClientComparator|. 
    \par 
    Another list of clients (\verb|remainingClients|) was created, pointing to the same client objects, in order to be able to access separately the total, and the remaining clients. This list is implemented using a \verb|LinkedList|, because it is more efficient for deletion/access only from/to the front of the list.
    \par
    In its \verb|call()| method lies the main loop of the simulation. The \verb|Time| object is used for comparing the current time to the time limit. The \verb|Client| classes are popped from the front of the \verb|remainingClients| list and dispatched to the scheduler when the condition following condition holds: $t_{arrival} = t_{simulation}$. At the end of each iteration the value of the current time is incremented, and the state of the queues is saved as a \verb|SimulationSnapshot|. 
    \par
    The simulation ends if the current time surpassed the time limit ($t_{simulation} > t_{simulation}^{MAX}$), or no more clients left processing, i.e. \verb|remainingClients| is empty and the \verb|scheduler| is not working. In this case the average time is calculated and returned together with the snapshots in the form of a \verb|SimulationResult|.
    
    \subsubsection{Scheduler}
    \label{sec:scheduler}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.5\linewidth]{figures/scheduler.png}
        \caption{Scheduler class}
        \label{fig:scheduler}
    \end{figure*}
    The \verb|Scheduler| connects the \verb|SimulationManager| to the \verb|Server| objects i.e. to the threads. 
    \par 
    Its main goal is to receive the clients one-by-one and dispatch them to one of the servers. The server selection is determined by a \verb|Strategy| object. One can select the strategy by using the \verb|changeStrategy(SelectionPolicy policy)| method, and passing a the corresponding policy value. If the \verb|dispatchClient(Client client)| 
    is called, the client is passed to the strategy, which adds it to the list.
    \par
    Another important part of the \verb|Scheduler| is the maintenance of the threads/servers. In the constructor $Q$ is given as parameter, and the scheduler creates this many \verb|Server| objects, subscribes them to the change in time and starts their threads. The \verb|getSnapshot()| method returns for all the servers an immutable list of the contents of their queue. This is called by the \verb|SimulationManager| for creating a new snapshot. The \verb|join()| method interrupts all threads and waits for their termination. And the \verb|isWorking()| method returns whether all threads finished their jobs (closed) and are waiting for new clients.
    
    \subsubsection{Server}
    \label{sec:server}
    \begin{figure*}[!htbp]
         \centering
         \includegraphics[width=0.5\linewidth]{figures/server.png}
         \caption{Server class}
         \label{fig:server}
    \end{figure*}
    The \verb|Server| class is the entity that represents the queue of \verb|Client| objects. It receives clients sequentially and waits for the duration of their \verb|serviceTime| attribute.
    \par
    It extends \verb|Thread| to run code in parallel to the main thread. It's \verb|run()| method is an infinite loop, and only ends when the thread is interrupted. In each iteration, the server takes the first element of the queue (removing it), sets the \verb|currentClient| field to this object, sets \verb|timeOfWakeup| to the current time offset by the service time of the client and the thread goes \verb|WAIT| status. Whenever the thread is notified, it exits the waiting stage, sets the \verb|currentClient| to \verb|null| and flags the client that it is served.
    \par
    There are two cases when the thread is in \verb|WAIT| state:
    \begin{enumerate}
     \item The queue is empty
     \item The server is processing a client
    \end{enumerate}
    \par
    To achieve the first case, a \verb|BlockingQueue| is used as the data structure for storing the enqueued clients. When the \verb|queue.take()| method is called, if the queue is empty, the thread will wait until a client arrives. Otherwise the client is popped from the queue and the thread goes to the next statement.
    \par
    To achieve the second case, the \verb|Server| implements the \verb|TimeListener| interface, and its \verb|timePassed()| method is called on each time pulse. Before going to waiting state, the \verb|timeOfWakeup| field is updated. On each time pulse, if the current time reached the time of wake up, the thread is notified, so it wakes up. The service time field of a client is interpreted in numbers of time pulses, thus $t_{wakeup} = t_{simulation} + t_{service}$. 
    \par
    To synchronise the the thread of the \verb|SimulationManager| with a \verb|Server| thread, the following measures are followed:
    \begin{itemize}
     \item The variables that are accessed by both threads are using a thread-safe types: \verb|AtomicInteger|, \verb|AtomicReference|, \verb|BlockingQueue|. \cite{blocking:2}
     \item The run method operates only while the timePassed method runs, i.e. only for the duration of the pulse.
    \end{itemize}
    \par
    To achieve the latter, a synchronized block is used on the critical section, where the \verb|timeOfWakeUp| is accessed by both threads. Furthermore, the master thread is blocked until the server thread reaches again the \verb|WAIT| state. This is also used in the \verb|getClients()| method, to ensure that the elements of the queue can be safely read.
    \par
    Because the \verb|take()| method of the \verb|BlockingQueue| removes the element from the queue (and no peek method exists with simmilar behaviour), the removed client must be stored in a field (\verb|currentClient|), so that the \verb|getClients()| method can read all the clients of the queue, including the one under processing. The method copies the queue to a list and appends the current client if there is one.
    \par
    The \verb|Server| stores in the \verb|totalWaitingPeriod| field the sum of the service times of the clients in its queue. In order to avoid traversing all the queues at each time pulse, the variable is increased each time an element is enqueued, and decremented at each pulse.
    \par 
    The server is considered to have finished all its tasks, when the queue is empty, it's in waiting state and the current time surpassed the \verb|timeOfWakeUp|, in other words, when it is waiting at the line \verb|Client client = queue.take();|.
    
    \subsection{Strategy}
    \label{sec:strategy}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.8\linewidth]{figures/strategy.png}
        \caption{Scheduler and the strategy package}
        \label{fig:strategy}
    \end{figure*}
    The classes that implement the \verb|Strategy| interface of the \verb|strategy| package are defining a way of distributing a client to the servers.
    \par
    The \verb|ShortestTimeStrategy| searches for the server with the minimum total waiting period and inserts the client into it.
    
    \subsection{Data transfer objects}
    \label{sec:dto}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=\linewidth]{figures/dto2.png}
        \caption{Data Transfer Object - classes}
        \label{fig:dto}
    \end{figure*}
    The data that which is used for file\verb|<->|method transfer are stored in data transfer objects, contained in the \verb|dto| package. These objects are immutable so they are thread safe.
    \par
    The \verb|SimulationConfig| class contains the fields represented in [\ref{sec:analysis}].
    \par
    The \verb|SimulationSnapshot| class contains:
    \begin{itemize}
     \item The simulation time, when the snapshot was made
     \item The list of clients that are not arrived yet
     \item The list of the elements of the queues for each server.
    \end{itemize}
    \par
    The \verb|SimulationResult| class contains:
    \begin{itemize}
     \item A list of \verb|SimulationSnapshot| corresponding to each time of simulation.
     \item The average waiting time at the end of the simulation
    \end{itemize}

    
    \subsection{Input and output}
    \label{sec:io}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=\linewidth]{figures/io.png}
        \caption{Transformer interface and the io package}
        \label{fig:io}
    \end{figure*}
    The problem of reading and writing the data transfer objects to text files was generalized to arbitrary objects and arbitrary input/output sources.
    
    The \verb|transformer| package contains the \verb|Transformer<T>| generic interface and its corresponding implementations for the three data transfer objects. The interface has two methods:
    \begin{itemize}
     \item \verb|String toString(T obj)| converts an object of generic type to a String
    \item \verb|T toObject(String string)| by parsing the string, converts it into an object of the generic type. \verb|InvalidInputFormatException| can be thrown if the string does not correspond the format which is required by the method.
    \end{itemize}
    \par
    The \verb|io| package contains the \verb|ObjectWriter| and \verb|ObjectReader| interfaces, which by receiving a transformer, it can write an object of the corresponding type into an output source, or it can read one from an input source. These interfaces are implemented by \verb|FileObjectWriter| and \verb|FileObjectReader|, thus specifying the input and output sources as files. But other implementations could use other modes of data storage, for example a database.
    
    \subsection{Validation}
    \label{sec:validation}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.5\linewidth]{figures/validation.png}
        \caption{validation package}
        \label{fig:validation}
    \end{figure*}
    The \verb|SimulationConfig| data transfer object is validated after the read from the file with the \verb|SimulationConfigValidatior| that throws \verb|InvalidInputFormatException| if the values are not valid.
    
    \section{Result}
    In order to debug the multi-threaded application besides the regular debugger, logs were used. In the final product, the logs were disable from the main class.
    \par
    To test the correct behaviour on edge cases, various test files were given as input, where out-of-range, invalid and large values were also represented. Also it was tested to not crash on non-existing input files.
    
    \section{Conclusion}
    The main objective was achieved by completing the secondary objectives.
    \par
    A generalized input-output mechanism were created to store and read objects from various sources, for example a text file. This can be reused in other applications requiring these operations.
    \par
    By using immutable data transfer objects one can ensure that the data will be received in the same state as it was created.
    \par
    By using a random client generator, t
    \par
    In this application the difference between a Callable and a Runnable/Thread object and in their use cases could be observed. The former returning a value after execution in parallel, while the latter can be used to endlessly work in the background.
    \par 
    Synchronisation proved to be an essential part of a multi-threaded application. In this project it was used in multiple ways: 
    \begin{itemize}
     \item Thread-safe collections, such as \verb|BlockingQueue| \cite{blocking:2}
     \item Atomic variables, such as \verb|AtomicInteger|, \verb|AtomicReference|
     \item Synchronized blocks on critical sections and wait-notify communications \cite{blocking:1}, such as in [\ref{sec:server}]. 
    \end{itemize}
    \par
    Design patterns were also needed to achieve the main objective:
    \begin{itemize}
     \item Singleton pattern was used to ensure a global time.
     \item Observer pattern \cite{observer}  was used to notify all the \verb|TimeListener| objects.
     \item The strategy pattern also proved to be very useful by providing a common umbrella to different types of algorithms.
    \end{itemize}
    \par
    The application can be further developed by:
    \begin{itemize}
     \item supporting different strategies and comparing them
     \item supporting the dynamic increasing and decreasing of the number of threads whenever it appears to be more efficient.
     \item providing a graphical user interface
    \end{itemize}


    \bibliography{bibliography}
    \bibliographystyle{plain}
    
\end{document}
