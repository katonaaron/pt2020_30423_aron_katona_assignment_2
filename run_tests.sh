#!/bin/bash

for file in in-test-*.txt
do
  echo Executing test "$file"
  outputFile=out"${file:2}"
  java -jar PT2020_Group_Aron_Katona_Assignment_2.jar "$file" "$outputFile"
  echo Result: "$outputFile"
done

