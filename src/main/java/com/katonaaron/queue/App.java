package com.katonaaron.queue;

import com.katonaaron.queue.dto.SimulationConfig;
import com.katonaaron.queue.dto.SimulationResult;
import com.katonaaron.queue.exception.InvalidInputFormatException;
import com.katonaaron.queue.io.FileObjectReader;
import com.katonaaron.queue.io.FileObjectWriter;
import com.katonaaron.queue.io.ObjectReader;
import com.katonaaron.queue.io.ObjectWriter;
import com.katonaaron.queue.simulation.SimulationManager;
import com.katonaaron.queue.transformer.SimulationConfigTransformer;
import com.katonaaron.queue.transformer.SimulationResultTransformer;
import com.katonaaron.queue.validation.SimulationConfigValidator;
import com.katonaaron.queue.validation.Validator;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.LogManager;

import static java.lang.System.exit;

public class App {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Required parameters: <input_path> <output_path>");
            exit(-1);
        }

        /* Disable logging */
        LogManager.getLogManager().reset();

        SimulationConfig config = readConfiguration(args[0]);
        if (config == null) {
            exit(-1);
        }

        FutureTask<SimulationResult> simulation = new FutureTask<>(new SimulationManager(config));
        Thread thread = new Thread(simulation);
        thread.start();

        try {
            thread.join();
            writeResult(args[1], simulation.get());
        } catch (InterruptedException e) {
            System.err.println("ERROR: interrupted: " + e);
            exit(-1);
        } catch (ExecutionException e) {
            System.err.println("ERROR: exception occurred during execution: " + e);
            exit(-1);
        }
    }

    private static SimulationConfig readConfiguration(String path) {
        Validator<SimulationConfig> validator = new SimulationConfigValidator();
        SimulationConfig config = null;

        try {
            ObjectReader<SimulationConfig> objectReader = new FileObjectReader<>(path);
            config = objectReader.read(new SimulationConfigTransformer());
            validator.validate(config);
        } catch (InvalidInputFormatException e) {
            System.err.println("ERROR: invalid input file format: " + e.getMessage());
            exit(-1);
        } catch (NoSuchFileException e) {
            System.err.println("ERROR: no such file: " + e.getMessage());
            exit(-1);
        } catch (IOException e) {
            System.err.println("ERROR: file could not be read: " + e);
            exit(-1);
        }

        return config;
    }

    private static void writeResult(String path, SimulationResult result) {
        ObjectWriter<SimulationResult> objectWriter = new FileObjectWriter<>(path);
        try {
            objectWriter.write(new SimulationResultTransformer(), result);
        } catch (IOException e) {
            System.err.println("ERROR: could not write to file: " + e);
        }
    }
}
