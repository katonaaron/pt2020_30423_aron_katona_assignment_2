package com.katonaaron.queue.client;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Client {
    private final int id;
    private final int arrivalTime;
    private final AtomicInteger serviceTime;
    private final AtomicBoolean isServed;
    private int waitingTime;

    public Client(int id, int arrivalTime, int serviceTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.serviceTime = new AtomicInteger(serviceTime);
        this.isServed = new AtomicBoolean(false);
        this.waitingTime = 0;
    }

    public int getId() {
        return id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime.get();
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public boolean isServed() {
        return this.isServed.get();
    }

    public void serve() {
        this.isServed.set(true);
    }

    public int getTotalWaitingTime() {
        return waitingTime + getServiceTime();
    }

    @Override
    public String toString() {
        return "(" + id +
                ", " + arrivalTime +
                ", " + serviceTime +
                ')';
    }
}
