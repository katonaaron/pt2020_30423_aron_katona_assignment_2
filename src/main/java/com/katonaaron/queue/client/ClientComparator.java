package com.katonaaron.queue.client;

import java.util.Comparator;

public class ClientComparator implements Comparator<Client> {
    @Override
    public int compare(Client o1, Client o2) {
        final int arrivalDiff = o1.getArrivalTime() - o2.getArrivalTime();
        return arrivalDiff == 0 ? o1.getServiceTime() - o2.getServiceTime() : arrivalDiff;
    }
}
