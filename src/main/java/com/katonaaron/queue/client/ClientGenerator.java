package com.katonaaron.queue.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class ClientGenerator {
    private static Random random = new Random();

    private ClientGenerator() {
    }

    public static List<Client> generateClients(int noOfClients, int minArrival, int maxArrival, int minService, int maxService) {
        List<Client> clients = new ArrayList<>();

        for (int i = 0; i < noOfClients; i++) {
            clients.add(
                    new Client(
                            i + 1,
                            getRandomInt(minArrival, maxArrival),
                            getRandomInt(minService, maxService)
                    )
            );
        }

        return clients;
    }

    private static int getRandomInt(int low, int high) {
        return random.nextInt(high - low + 1) + low;
    }
}
