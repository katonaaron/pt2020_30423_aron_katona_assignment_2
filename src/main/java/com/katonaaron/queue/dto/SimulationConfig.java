package com.katonaaron.queue.dto;

import java.util.List;

public class SimulationConfig {
    private final int numberOfClients;
    private final int numberOfServers;
    private final int timeLimit;
    private final int minArrivalTime;
    private final int maxArrivalTime;
    private final int minServiceTime;
    private final int maxServiceTime;

    public SimulationConfig(List<Integer> arg) {
        this.numberOfClients = arg.get(0);
        this.numberOfServers = arg.get(1);
        this.timeLimit = arg.get(2);
        this.minArrivalTime = arg.get(3);
        this.maxArrivalTime = arg.get(4);
        this.minServiceTime = arg.get(5);
        this.maxServiceTime = arg.get(6);
    }

    public SimulationConfig(int numberOfClients, int numberOfServers, int timeLimit, int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime) {
        this.numberOfClients = numberOfClients;
        this.numberOfServers = numberOfServers;
        this.timeLimit = timeLimit;
        this.minArrivalTime = minArrivalTime;
        this.maxArrivalTime = maxArrivalTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
    }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public int getNumberOfServers() {
        return numberOfServers;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public int getMinArrivalTime() {
        return minArrivalTime;
    }

    public int getMaxArrivalTime() {
        return maxArrivalTime;
    }

    public int getMinServiceTime() {
        return minServiceTime;
    }

    public int getMaxServiceTime() {
        return maxServiceTime;
    }

    @Override
    public String toString() {
        return "SimulationConfig{" +
                "numberOfClients=" + numberOfClients +
                ", numberOfServers=" + numberOfServers +
                ", simulationInterval=" + timeLimit +
                ", minArrivalTime=" + minArrivalTime +
                ", maxArrivalTime=" + maxArrivalTime +
                ", minServiceTime=" + minServiceTime +
                ", maxServiceTime=" + maxServiceTime +
                '}';
    }
}
