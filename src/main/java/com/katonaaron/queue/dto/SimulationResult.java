package com.katonaaron.queue.dto;

import java.util.List;

public class SimulationResult {
    private final List<SimulationSnapshot> snapshots;
    private final double averageWaitingTime;

    public SimulationResult(List<SimulationSnapshot> snapshots, double averageWaitingTime) {
        this.snapshots = List.copyOf(snapshots);
        this.averageWaitingTime = averageWaitingTime;
    }

    public List<SimulationSnapshot> getSnapshots() {
        return snapshots;
    }

    public double getAverageWaitingTime() {
        return averageWaitingTime;
    }
}
