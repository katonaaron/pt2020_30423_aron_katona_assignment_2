package com.katonaaron.queue.dto;

import com.katonaaron.queue.client.Client;

import java.util.List;

public class SimulationSnapshot {
    private final int time;
    private final List<Client> waitingClients;
    private final List<List<Client>> queues;

    public SimulationSnapshot(int time, List<Client> waitingClients, List<List<Client>> queues) {
        this.time = time;
        this.waitingClients = List.copyOf(waitingClients);
        this.queues = List.copyOf(queues);
    }

    public int getTime() {
        return time;
    }

    public List<Client> getWaitingClients() {
        return waitingClients;
    }

    public List<List<Client>> getQueues() {
        return queues;
    }
}
