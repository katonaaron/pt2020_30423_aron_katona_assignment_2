package com.katonaaron.queue.exception;

public class InvalidInputFormatException extends Exception {

    public InvalidInputFormatException() {
    }

    public InvalidInputFormatException(String message) {
        super(message);
    }

    public InvalidInputFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidInputFormatException(Throwable cause) {
        super(cause);
    }
}
