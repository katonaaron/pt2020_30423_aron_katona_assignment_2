package com.katonaaron.queue.io;

import com.katonaaron.queue.exception.InvalidInputFormatException;
import com.katonaaron.queue.transformer.Transformer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileObjectReader<T> implements ObjectReader<T> {
    private Path path;

    public FileObjectReader(String path) {
        this.path = Path.of(path);
    }

    @Override
    public T read(Transformer<T> transformer) throws IOException, InvalidInputFormatException {
        return transformer.toObject(Files.readString(path));
    }
}

