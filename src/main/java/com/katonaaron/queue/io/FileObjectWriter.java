package com.katonaaron.queue.io;

import com.katonaaron.queue.transformer.Transformer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileObjectWriter<T> implements ObjectWriter<T> {
    private Path path;

    public FileObjectWriter(String path) {
        this.path = Path.of(path);
    }

    @Override
    public void write(Transformer<T> transformer, T obj) throws IOException {
        Files.writeString(path, transformer.toString(obj));
    }
}
