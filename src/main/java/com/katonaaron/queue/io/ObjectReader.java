package com.katonaaron.queue.io;

import com.katonaaron.queue.exception.InvalidInputFormatException;
import com.katonaaron.queue.transformer.Transformer;

import java.io.IOException;

public interface ObjectReader<T> {
    T read(Transformer<T> transformer) throws IOException, InvalidInputFormatException;
}
