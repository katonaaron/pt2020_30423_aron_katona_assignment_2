package com.katonaaron.queue.io;

import com.katonaaron.queue.transformer.Transformer;

import java.io.IOException;

public interface ObjectWriter<T> {
    void write(Transformer<T> transformer, T obj) throws IOException;
}
