package com.katonaaron.queue.simulation;

import com.katonaaron.queue.client.Client;
import com.katonaaron.queue.strategy.SelectionPolicy;
import com.katonaaron.queue.strategy.ShortestTimeStrategy;
import com.katonaaron.queue.strategy.Strategy;
import com.katonaaron.queue.time.Time;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Scheduler {
    private final List<Server> servers;
    private Strategy strategy;

    public Scheduler(int numberOfServers) {
        servers = new ArrayList<>();

        for (int i = 0; i < numberOfServers; i++) {
            Server server = new Server("Server " + i);
            servers.add(server);
            Time.getInstance().addTimeListener(server);
            server.start();
        }

        changeStrategy(SelectionPolicy.SHORTEST_TIME);
    }

    public void changeStrategy(SelectionPolicy policy) {
        switch (policy) {
            case SHORTEST_TIME:
                strategy = new ShortestTimeStrategy();
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public void dispatchClient(Client client) {
        strategy.addClient(servers, client);
    }

    public void join() throws InterruptedException {
        servers.forEach(Server::interrupt);
        for (Server server : servers) {
            server.join();
        }
    }

    public boolean isWorking() {
        return !servers.stream().allMatch(Server::isFinished);
    }

    public List<List<Client>> getSnapshot() {
        return servers.stream()
                .map(Server::getClients)
                .collect(Collectors.toList());
    }
}
