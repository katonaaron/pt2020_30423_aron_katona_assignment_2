package com.katonaaron.queue.simulation;

import com.katonaaron.queue.client.Client;
import com.katonaaron.queue.time.Time;
import com.katonaaron.queue.time.TimeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

public class Server extends Thread implements TimeListener {
    private final BlockingQueue<Client> queue;
    private final AtomicInteger timeOfWakeUp;
    private final AtomicReference<Client> currentClient;
    private final Logger logger;
    private int totalWaitingPeriod;


    public Server(String name) {
        super(name);
        queue = new LinkedBlockingQueue<>();
        totalWaitingPeriod = 0;
        timeOfWakeUp = new AtomicInteger(0);
        currentClient = new AtomicReference<>(null);
        logger = Logger.getGlobal();
    }


    public void timePassed(int time) {
        totalWaitingPeriod = totalWaitingPeriod > 0 ? totalWaitingPeriod - 1 : 0;

        if (isFinished()) {
            return;
        }

        synchronized (this) {
            if (timeOfWakeUp.get() <= time) {
                this.notify();
            }
        }
        while (this.getState() != State.WAITING) ;
    }

    public boolean isFinished() {
        return queue.isEmpty() && this.getState() == State.WAITING && timeOfWakeUp.get() < Time.getInstance().getTime();
    }

    public void addClientToQueue(Client client) {
        queue.add(client);
        totalWaitingPeriod += client.getServiceTime();
    }

    @Override
    public void run() {
        boolean exit = false;
        while (!exit) {
            try {
                Client client = queue.take();

                logger.info("Time " + Time.getInstance().getTime() + ": "
                        + Thread.currentThread().getName() + ": Client received: " + client);

                synchronized (this) {
                    currentClient.set(client);
                    timeOfWakeUp.set(client.getServiceTime() + Time.getInstance().getTime());
                    wait();
                    currentClient.set(null);
                }

                client.serve();
                logger.info("Time " + Time.getInstance().getTime() + ": "
                        + Thread.currentThread().getName() + ": Client served: " + client);

            } catch (InterruptedException ignored) {
                logger.info(Thread.currentThread().getName() + " interrupted");
                exit = true;
            }
        }
    }

    public int getTotalWaitingPeriod() {
        return totalWaitingPeriod;
    }

    public List<Client> getClients() {
        while (this.getState() != State.WAITING) ;
        Client client = currentClient.get();
        List<Client> clients = new ArrayList<>(List.copyOf(queue));

        if (client != null) {
            clients.add(0, new Client(client.getId(), client.getArrivalTime(), timeOfWakeUp.get() - Time.getInstance().getTime() + 1));
        }

        return clients;
    }
}
