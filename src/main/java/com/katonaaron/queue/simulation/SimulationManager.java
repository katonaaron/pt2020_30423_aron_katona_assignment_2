package com.katonaaron.queue.simulation;

import com.katonaaron.queue.client.Client;
import com.katonaaron.queue.client.ClientComparator;
import com.katonaaron.queue.client.ClientGenerator;
import com.katonaaron.queue.dto.SimulationConfig;
import com.katonaaron.queue.dto.SimulationResult;
import com.katonaaron.queue.dto.SimulationSnapshot;
import com.katonaaron.queue.time.Time;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

public class SimulationManager implements Callable<SimulationResult> {
    private final Time time;
    private final Scheduler scheduler;
    private final List<Client> clients;
    private final List<Client> remainingClients;
    private final int timeLimit;

    private final List<SimulationSnapshot> snapshots;

    public SimulationManager(SimulationConfig config) {
        this.timeLimit = config.getTimeLimit();

        clients = ClientGenerator.generateClients(
                config.getNumberOfClients(),
                config.getMinArrivalTime(),
                config.getMaxArrivalTime(),
                config.getMinServiceTime(),
                config.getMaxServiceTime()
        );
        clients.sort(new ClientComparator());
        remainingClients = new LinkedList<>(clients);
        Logger.getGlobal().info("Clients generated: " + clients);

        scheduler = new Scheduler(config.getNumberOfServers());
        snapshots = new ArrayList<>();
        time = Time.getInstance();
    }

    @Override
    public SimulationResult call() throws InterruptedException {
        if (time.getTime() > timeLimit)
            time.reset();

        while (time.getTime() <= timeLimit && (!remainingClients.isEmpty() || scheduler.isWorking())) {
            while (!remainingClients.isEmpty() && remainingClients.get(0).getArrivalTime() <= time.getTime()) {
                scheduler.dispatchClient(remainingClients.get(0));
                remainingClients.remove(0);
            }

            time.tick();
            snapshots.add(new SimulationSnapshot(
                    time.getTime() - 1,
                    remainingClients,
                    scheduler.getSnapshot()
            ));
        }

        scheduler.join();
        return new SimulationResult(snapshots, getAverageWaitingTime());
    }

    private double getAverageWaitingTime() {
        return clients.stream()
                .filter(Client::isServed)
                .map(Client::getTotalWaitingTime)
                .mapToInt(x -> x)
                .average()
                .orElse(0);
    }
}
