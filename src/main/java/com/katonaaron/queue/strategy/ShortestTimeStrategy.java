package com.katonaaron.queue.strategy;

import com.katonaaron.queue.client.Client;
import com.katonaaron.queue.simulation.Server;
import com.katonaaron.queue.time.Time;

import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

public class ShortestTimeStrategy implements Strategy {
    @Override
    public void addClient(List<Server> servers, Client client) {
        Server server = servers.stream()
                .min(Comparator.comparingInt(Server::getTotalWaitingPeriod))
                .orElseThrow();
        client.setWaitingTime(server.getTotalWaitingPeriod());

        Logger.getLogger("Time " + Time.getInstance().getTime() + ": Server " + servers.indexOf(server)
                + ": Client inserted: " + client + " Waiting period: " + server.getTotalWaitingPeriod());

        server.addClientToQueue(client);
    }
}
