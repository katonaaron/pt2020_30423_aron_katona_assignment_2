package com.katonaaron.queue.strategy;

import com.katonaaron.queue.client.Client;
import com.katonaaron.queue.simulation.Server;

import java.util.List;

public interface Strategy {
    void addClient(List<Server> servers, Client client);
}
