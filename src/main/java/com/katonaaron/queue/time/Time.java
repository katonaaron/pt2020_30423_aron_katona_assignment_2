package com.katonaaron.queue.time;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Time {
    private static Time instance;
    private final List<TimeListener> listeners;
    private final AtomicInteger time;

    private Time() {
        listeners = new ArrayList<>();
        time = new AtomicInteger(0);
    }

    public static Time getInstance() {
        if (instance == null) {
            instance = new Time();
        }
        return instance;
    }

    public int getTime() {
        return time.get();
    }

    public void addTimeListener(TimeListener listener) {
        listeners.add(listener);
    }

    public void tick() {
        listeners.forEach(listener -> listener.timePassed(time.get()));
        time.incrementAndGet();
    }

    public void reset() {
        time.set(0);
    }

}
