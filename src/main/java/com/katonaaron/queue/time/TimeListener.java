package com.katonaaron.queue.time;

public interface TimeListener {
    void timePassed(int time);
}
