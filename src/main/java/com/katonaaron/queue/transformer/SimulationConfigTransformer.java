package com.katonaaron.queue.transformer;

import com.katonaaron.queue.dto.SimulationConfig;
import com.katonaaron.queue.exception.InvalidInputFormatException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SimulationConfigTransformer implements Transformer<SimulationConfig> {
    @Override
    public SimulationConfig toObject(String string) throws InvalidInputFormatException {
        List<Integer> values;
        try {
            values = Arrays.stream(string.split("[\\n,]"))
                    .map(Integer::valueOf)
                    .collect(Collectors.toList());
        } catch (NumberFormatException e) {
            throw new InvalidInputFormatException("Input data is not a number", e);
        }


        if (values.size() != SimulationConfig.class.getDeclaredFields().length) {
            throw new InvalidInputFormatException("Invalid number of input data");
        }

        return new SimulationConfig(values);
    }
}
