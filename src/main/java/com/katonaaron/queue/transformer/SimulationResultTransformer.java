package com.katonaaron.queue.transformer;

import com.katonaaron.queue.dto.SimulationResult;

import java.util.stream.Collectors;

public class SimulationResultTransformer implements Transformer<SimulationResult> {
    private final SimulationSnapshotTransformer snapshotTransformer;

    public SimulationResultTransformer() {
        snapshotTransformer = new SimulationSnapshotTransformer();
    }

    @Override
    public String toString(SimulationResult simulationResult) {
        return simulationResult.getSnapshots().stream()
                .map(snapshotTransformer::toString)
                .collect(Collectors.joining("\n"))
                + "\nAverage waiting time: " + simulationResult.getAverageWaitingTime();
    }

    @Override
    public SimulationResult toObject(String string) {
        throw new UnsupportedOperationException();
    }
}
