package com.katonaaron.queue.transformer;

import com.katonaaron.queue.client.Client;
import com.katonaaron.queue.dto.SimulationSnapshot;

import java.util.List;


public class SimulationSnapshotTransformer implements Transformer<SimulationSnapshot> {
    @Override
    public String toString(SimulationSnapshot snapshot) {

        return "Time " + snapshot.getTime()
                + "\nWaiting clients: " + listToString(snapshot.getWaitingClients())
                + "\n" + queuesToString(snapshot.getQueues());
    }

    @Override
    public SimulationSnapshot toObject(String string) {
        throw new UnsupportedOperationException();
    }

    private String queuesToString(List<List<Client>> queues) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < queues.size(); i++) {
            builder.append("Queue ")
                    .append(i)
                    .append(": ");

            if (!queues.get(i).isEmpty()) {
                builder.append(listToString(queues.get(i)))
                        .append(";");
            } else {
                builder.append("closed");
            }

            builder.append("\n");
        }
        return builder.toString();
    }

    private <T> String listToString(List<T> list) {
        return list.stream().map(T::toString).reduce((a, b) -> a + "; " + b).orElse("");
    }
}
