package com.katonaaron.queue.transformer;

import com.katonaaron.queue.exception.InvalidInputFormatException;

public interface Transformer<T> {
    default String toString(T obj) {
        return obj.toString();
    }

    T toObject(String string) throws InvalidInputFormatException;
}
