package com.katonaaron.queue.validation;

import com.katonaaron.queue.dto.SimulationConfig;
import com.katonaaron.queue.exception.InvalidInputFormatException;

public class SimulationConfigValidator implements Validator<SimulationConfig> {
    @Override
    public void validate(SimulationConfig config) throws InvalidInputFormatException {
        if (config.getNumberOfServers() <= 0) {
            throw new InvalidInputFormatException("The number of servers must be a positive integer");
        }

        if (config.getTimeLimit() < 0
                || config.getMaxArrivalTime() < 0
                || config.getMinArrivalTime() < 0
                || config.getMaxServiceTime() < 0
                || config.getMinServiceTime() < 0
                || config.getNumberOfClients() < 0
        ) {
            throw new InvalidInputFormatException("The numbers must be non-negative integers");
        }

        if (config.getMaxArrivalTime() < config.getMinArrivalTime()
                || config.getMaxServiceTime() < config.getMinServiceTime()
        ) {
            throw new InvalidInputFormatException("The ranges must be increasing");
        }
    }
}
