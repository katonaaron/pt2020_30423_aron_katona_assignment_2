package com.katonaaron.queue.validation;

import com.katonaaron.queue.exception.InvalidInputFormatException;

public interface Validator<T> {
    void validate(T obj) throws InvalidInputFormatException;
}
